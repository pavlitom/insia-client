<?php
namespace Pavlitom\InsiaClient;

final class SellerAdminTest extends AbstractTest
{
	public function testSuccessfulCreateSellerAdmin(): void
	{
		$this->mockResponseStatusCode(200)
			->mockResponse([
					'id' => '4a54f64sd54fas5d4f5a4sdf5',
				]
			);

		$this->httpClient->shouldReceive('post')
			->once()
			->with(self::API_URL . '/selleradmin', [
					'headers' => [
						'Content-Type' => 'application/json',
						'sig' => 'abcd123456',
					],
					'json' => [
						'firstname' => 'John',
						'lastname' => 'Doe',
					],
				]
			)
			->andReturn($this->responseMock);

		$sellerAdminId = $this->insiaClient->setSig('abcd123456')
			->sellerAdmin()
			->create('John', 'Doe');

		$this->assertEquals('4a54f64sd54fas5d4f5a4sdf5', $sellerAdminId);
	}

	public function testSuccessfulMoveSellerAdmin(): void
	{
		$this->mockResponseStatusCode(200);

		$this->httpClient->shouldReceive('put')
			->once()
			->with(self::API_URL . '/selleradmin/detail/1234', [
					'headers' => [
						'Content-Type' => 'application/json',
						'sig' => 'abcd123456',
					],
					'json' => [
						'firstname' => 'John',
						'lastname' => 'Doe',
						'branch_offices_number' => 1040,
					],
				]
			)
			->andReturn($this->responseMock);

		$moved = $this->insiaClient->setSig('abcd123456')
			->sellerAdmin()
			->moveToStore(1234, 'John', 'Doe', 1040);

		$this->assertTrue($moved);
	}
}
