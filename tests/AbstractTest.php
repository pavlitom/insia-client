<?php
namespace Pavlitom\InsiaClient;

use GuzzleHttp\Client as GuzzleHttpClient;
use Mockery;
use Mockery\Adapter\Phpunit\MockeryPHPUnitIntegration;
use Mockery\MockInterface;
use PHPUnit\Framework\TestCase;
use Psr\Http\Message\ResponseInterface;

abstract class AbstractTest extends TestCase
{
    use MockeryPHPUnitIntegration;

    public const API_URL = 'http://localhost';

    /** @var InsiaClient */
    protected $insiaClient;

    /** @var MockInterface|GuzzleHttpClient */
    protected $httpClient;

    /** @var MockInterface */
    protected $responseMock;

    protected function setUp(): void
    {
        $this->httpClient = Mockery::mock(GuzzleHttpClient::class);
        $this->insiaClient = new InsiaClient(self::API_URL, $this->httpClient);
        $this->responseMock = Mockery::mock(ResponseInterface::class);
    }

    protected function mockResponseStatusCode(int $statusCode): self
    {
        $this->responseMock->shouldReceive('getStatusCode')
            ->once()
            ->andReturn($statusCode);

        return $this;
    }

    /**
     * @param mixed $data
     */
    protected function mockResponse($data = []): self
    {
        $this->responseMock->shouldReceive('getBody')
            ->once()
            ->andReturn(json_encode($data));

        return $this;
    }
}
