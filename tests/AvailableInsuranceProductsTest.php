<?php
namespace Pavlitom\InsiaClient;

use Illuminate\Support\Arr;
use Pavlitom\InsiaClient\Model\Product;

final class AvailableInsuranceProductsTest extends AbstractTest
{
    public function testRespondWithErrorProductNotFound(): void
    {
        $this->mockResponseStatusCode(200)
            ->mockResponse([
                    'Errors:' => [
                        [
                            'code' => 1005,
                            'message' => 'Insurance product not found.',
                            'fields' => null,
                        ],
                    ],
                ]
            );

        $this->httpClient->shouldReceive('get')
            ->once()
            ->with(
                self::API_URL . '/available-insurance-products?' . http_build_query([
                    'deviceId' => 248,
                    'currency' => 'EUR',
                    'productPrice' => 859.50,
                ]),
                [
                    'headers' => [
                        'Content-Type' => 'application/json',
                        'sig' => 'abcd123456',
                    ],
                ]
            )
            ->andReturn($this->responseMock);

        $availableProducts = $this->insiaClient->setSig('abcd123456')
            ->availableInsuranceProducts()
            ->get(248, 'EUR', 859.50);

        $this->assertCount(0, $availableProducts);
    }

    public function testRespondWithAvailableProducts(): void
    {
        $this->mockResponseStatusCode(200)
            ->mockResponse([
                [
                    [
                        'id' => 91,
                        'title' => 'AXA - Náhodné poškodenie a odcudzenie 1 rok',
                        'sku' => 'SKAXAN1-12',
                        'type' => [
                            'id' => 91,
                            'title' => 'AXA - Náhodné poškodenie a odcudzenie 1 rok',
                        ],
                        'price' => [
                            'zoneId' => 11483,
                            'priceFloat' => 42.57,
                            'value' => 42.57,
                            'currency' => [
                                'code' => 'EUR',
                            ],
                        ],
                    ],
                ],
            ]);

        $this->httpClient->shouldReceive('get')
            ->once()
            ->with(
                self::API_URL . '/available-insurance-products?' . http_build_query([
                    'deviceId' => 248,
                    'currency' => 'EUR',
                    'productPrice' => 859.50,
                ]),
                [
                    'headers' => [
                        'Content-Type' => 'application/json',
                        'sig' => 'abcd123456',
                    ],
                ]
            )
            ->andReturn($this->responseMock);

        $availableProducts = $this->insiaClient->setSig('abcd123456')
            ->availableInsuranceProducts()
            ->get(248, 'EUR', 859.50);

        $this->assertCount(1, $availableProducts);

        $product = Arr::first($availableProducts);
        $this->assertInstanceOf(Product::class, $product);
        $this->assertEquals(91, $product->getId());
        $this->assertEquals('EUR', $product->getCurrency());
        $this->assertEquals(248, $product->getDeviceId());
        $this->assertEquals('SKAXAN1-12', $product->getGroup());
        $this->assertEquals(42.57, $product->getPrice());
        $this->assertEquals('AXA - Náhodné poškodenie a odcudzenie 1 rok', $product->getTitle());
    }
}
