<?php
namespace Pavlitom\InsiaClient;

use GuzzleHttp\Client as HttpClient;

class InsiaClientFactory
{
	public static function create(string $apiUri): InsiaClient
	{
		return new InsiaClient($apiUri, new HttpClient());
	}
}
