<?php
namespace Pavlitom\InsiaClient;

use Pavlitom\InsiaClient\Endpoint\AvailableInsuranceProducts;
use Pavlitom\InsiaClient\Endpoint\SellerAdmin;
use Psr\Log\LoggerInterface;

interface InsiaClientInterface
{
	public function availableInsuranceProducts(): AvailableInsuranceProducts;

	public function sellerAdmin(): SellerAdmin;

	public function setLogger(LoggerInterface $logger): void;

	public function setLoggerMessage(string $loggerMessage): void;

	public function setSig(string $sig): InsiaClient;
}
