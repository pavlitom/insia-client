<?php
namespace Pavlitom\InsiaClient;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use Psr\Http\Message\ResponseInterface;

class HttpRequest
{
	const HTTP_STATUS_OK = 200;

	/** @var Client */
	private $httpClient;

	/** @var string */
	private $apiUrl;

	/** @var string */
	private $apiKey;

	public function __construct(Client $httpClient, string $apiUrl, string $apiKey)
	{
		$this->apiKey = $apiKey;
		$this->apiUrl = rtrim($apiUrl, '/');
		$this->httpClient = $httpClient;
	}

	private function endpointUrl(string $endpoint): string
	{
		return $this->apiUrl . '/' . ltrim($endpoint, '/');
	}

	/**
	 * @throws InsiaClientException
	 */
	private function processRequestException(RequestException $e): void
	{
		if (!$response = $e->getResponse()) {
			throw new InsiaClientException(['Empty response!'], 410);
		}

		$statusCode = $response->getStatusCode();
		$contents = $response->getBody()->__toString();

		if (empty($contents)) {
			throw new InsiaClientException(['Empty response!'], $statusCode);
		}

		/** @var string[]|array<string, mixed>|null $body */
		$body = json_decode($contents, true);

		if (isset($body['message'])) {
			throw new InsiaClientException([$body['message']], $statusCode);
		}

		if (isset($body['Errors:'])) {
			throw new InsiaClientException((array) $body['Errors:'], $statusCode);
		}

		throw new InsiaClientException(['Empty response!'], $statusCode);
	}

	/**
	 * @param mixed $body
	 *
	 * @return array<string, mixed>
	 */
	private function requestContent($body): array
	{
		$content = [
			'headers' => $this->requestHeaders(),
		];

		if ($body) {
			$content['json'] = $body;
		}

		return $content;
	}

	/**
	 * @return array<string,string>
	 */
	private function requestHeaders(): array
	{
		return [
			'Content-Type' => 'application/json',
			'sig' => $this->apiKey,
		];
	}

	/**
	 * @param mixed $body
	 *
	 * @throws InsiaClientException
	 */
	public function get(string $endpoint, $body = null): ResponseInterface
	{
		try {
			return $this->httpClient->get(
				$this->endpointUrl($endpoint),
				$this->requestContent($body)
			);
		} catch (RequestException $e) {
			$this->processRequestException($e);
		}
	}

	/**
	 * @param mixed $body
	 *
	 * @throws InsiaClientException
	 */
	public function post(string $endpoint, $body): ResponseInterface
	{
		try {
			return $this->httpClient->post(
				$this->endpointUrl($endpoint),
				$this->requestContent($body)
			);
		} catch (RequestException $e) {
			$this->processRequestException($e);
		}
	}

	/**
	 * @param mixed $body
	 *
	 * @throws InsiaClientException
	 */
	public function put(string $endpoint, $body): ResponseInterface
	{
		try {
			return $this->httpClient->put(
				$this->endpointUrl($endpoint),
				$this->requestContent($body)
			);
		} catch (RequestException $e) {
			$this->processRequestException($e);
		}
	}
}
