<?php
namespace Pavlitom\InsiaClient\Model;

class Product
{
	/** @var int */
	private $id;

	/** @var int */
	private $deviceId;

	/** @var string */
	private $title;

	/** @var string */
	private $group;

	/** @var float */
	private $price;

	/** @var string */
	private $currency;

	public function __construct(int $id, int $deviceId, string $title, string $group, float $price, string $currency)
	{
		$this->id = $id;
		$this->deviceId = $deviceId;
		$this->title = $title;
		$this->group = $group;
		$this->price = $price;
		$this->currency = $currency;
	}

	public function getCurrency(): string
	{
		return $this->currency;
	}

	public function getDeviceId(): int
	{
		return $this->deviceId;
	}

	public function getGroup(): string
	{
		return $this->group;
	}

	public function getId(): int
	{
		return $this->id;
	}

	public function getPrice(): float
	{
		return $this->price;
	}

	public function getTitle(): string
	{
		return $this->title;
	}
}
