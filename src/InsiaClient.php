<?php
namespace Pavlitom\InsiaClient;

use GuzzleHttp\Client as HttpClient;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\MessageFormatter;
use GuzzleHttp\Middleware;
use Pavlitom\InsiaClient\Endpoint\AvailableInsuranceProducts;
use Pavlitom\InsiaClient\Endpoint\SellerAdmin;
use Psr\Log\LoggerInterface;

class InsiaClient implements InsiaClientInterface
{
	/** @var string|null */
	private $apiKey;

	/** @var string */
	private $apiUri;

	/** @var HttpClient */
	private $httpClient;

	/** @var string */
	private $loggerMessage = ">>>>>>>>\n"
	. "{req_headers}\n>>>>>>>>\n"
	. "{request}\n<<<<<<<<\n"
	. "{res_headers}\n<<<<<<<<\n"
	. "{response}\n--------\n"
	. "{error}";

	public function __construct(string $apiUri, HttpClient $httpClient)
	{
		$this->apiUri = $apiUri;
		$this->httpClient = $httpClient;
	}

	public function setSig(string $sig): self
	{
		$this->apiKey = $sig;

		return $this;
	}

	private function createHttpRequest(): HttpRequest
	{
		if (!$this->apiKey) {
			throw new InsiaClientException(['Sig is not set!'], 500);
		}

		return new HttpRequest($this->httpClient, $this->apiUri, $this->apiKey);
	}

	/**
	 * @throws InsiaClientException
	 */
	public function availableInsuranceProducts(): AvailableInsuranceProducts
	{
		return new AvailableInsuranceProducts($this->createHttpRequest());
	}

	/**
	 * @throws InsiaClientException
	 */
	public function sellerAdmin(): SellerAdmin
	{
		return new SellerAdmin($this->createHttpRequest());
	}

	public function setLogger(LoggerInterface $logger): void
	{
		/** @var HandlerStack $handler */
		$handler = $this->httpClient->getConfig('handler');
		$handler->push(Middleware::log($logger, new MessageFormatter($this->loggerMessage)));
	}

	public function setLoggerMessage(string $loggerMessage): void
	{
		$this->loggerMessage = $loggerMessage;
	}
}
