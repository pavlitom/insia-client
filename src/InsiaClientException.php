<?php
namespace Pavlitom\InsiaClient;

use Exception;
use Throwable;

class InsiaClientException extends Exception
{
	/** @var string[]|array<array<string,string>> */
	private $errors;

	/**
	 * @param string[]|array<array<string,string>> $errors
	 */
	public function __construct(array $errors, int $code = 0, Throwable $previous = null)
	{
		$this->errors = $errors;

		parent::__construct($this->generateMessage(), $code, $previous);
	}

	private function generateMessage(): string
	{
		$message = '';

		foreach ($this->errors as $error) {
			if (isset($error['message'])) {
				$message .= (empty($message) ? '' : ', ') . $error['message'];
			} else {
				if (is_string($error)) {
					$message .= (empty($message) ? '' : ', ') . $error;
				}
			}
		}

		return $message;
	}
}
