<?php
namespace Pavlitom\InsiaClient\Endpoint;

use Pavlitom\InsiaClient\HttpRequest;
use Pavlitom\InsiaClient\InsiaClientException;

class SellerAdmin extends BaseEndpoint
{
	const BASE_ENDPOINT = 'selleradmin';

	/**
	 * @throws InsiaClientException
	 */
	public function create(string $firstname, string $lastname): ?string
	{
		$response = $this->httpRequest->post(self::BASE_ENDPOINT, [
			'firstname' => $firstname,
			'lastname' => $lastname,
		]);

		if ($response->getStatusCode() == HttpRequest::HTTP_STATUS_OK) {
			/** @var array{'id': int}|null $data */
			$data = json_decode((string) $response->getBody(), true);

			if (isset($data['id'])) {
				return (string) $data['id'];
			}
		}

		return null;
	}

	/**
	 * @throws InsiaClientException
	 */
	public function moveToStore(int $userId, string $firstname, string $lastname, int $storeId): bool
	{
		$response = $this->httpRequest->put(self::BASE_ENDPOINT . '/detail/' . $userId, [
			'firstname' => $firstname,
			'lastname' => $lastname,
			'branch_offices_number' => $storeId,
		]);

		if ($response->getStatusCode() == HttpRequest::HTTP_STATUS_OK) {
			return true;
		}

		return false;
	}
}
