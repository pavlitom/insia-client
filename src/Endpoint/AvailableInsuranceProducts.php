<?php
namespace Pavlitom\InsiaClient\Endpoint;

use Pavlitom\InsiaClient\HttpRequest;
use Pavlitom\InsiaClient\InsiaClientException;
use Pavlitom\InsiaClient\Model\Product;

class AvailableInsuranceProducts extends BaseEndpoint
{
	const BASE_ENDPOINT = 'available-insurance-products';

	/**
	 * @return Product[]
	 * @throws InsiaClientException
	 */
	public function get(int $deviceId, string $currency, float $productPrice): array
	{
		$params = http_build_query([
			'deviceId' => $deviceId,
			'currency' => $currency,
			'productPrice' => $productPrice,
		]);

		$response = $this->httpRequest->get(self::BASE_ENDPOINT . '?' . $params);

		if ($response->getStatusCode() !== HttpRequest::HTTP_STATUS_OK) {
			return [];
		}

		$data = (string) $response->getBody();

		if (empty($data)) {
			return [];
		}

		$products = json_decode($data, true);

		if (null === $products || JSON_ERROR_NONE !== json_last_error() || !is_array($products)) {
			return [];
		}

		if (isset($products['Errors:'])) {
			// Insurance product not found.
			return [];
		}

		$products = reset($products);

		return collect($products)
			->transform(function (array $product) use ($deviceId) {
				return new Product(
					(int) $product['id'],
					$deviceId,
					(string) $product['title'],
					(string) $product['sku'],
					(float) $product['price']['priceFloat'],
					(string) $product['price']['currency']['code']
				);
			})
			->toArray();
	}
}
