<?php
namespace Pavlitom\InsiaClient\Endpoint;

use Pavlitom\InsiaClient\HttpRequest;

abstract class BaseEndpoint
{
	/** @var HttpRequest */
	protected $httpRequest;

	public function __construct(HttpRequest $httpRequest)
	{
		$this->httpRequest = $httpRequest;
	}
}
